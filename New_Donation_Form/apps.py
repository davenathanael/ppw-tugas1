from django.apps import AppConfig


class NewDonationFormConfig(AppConfig):
    name = 'New_Donation_Form'
