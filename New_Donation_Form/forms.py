from django import forms
from data.models import Event, User
from django.core.exceptions import ValidationError
# Donation Form Visualization

error_messages = {'required' : 'This field must be entered'}




attrs_name ={
    'class' : 'form-control',
    'type' : 'text',
    'placeholder' : 'Input your name here',
}

attrs_email ={
    'class' : 'form-control',
    'type' : 'email',
    'placeholder' : 'Input your e-mail here'
}

attrs_money ={
    'class' : 'form-control',
    'type' : 'text',
    'placeholder' : 'RpXX.XXX.XXX',
}

attrs_anon ={
    'class' : 'form-control',
    'type' : 'checkbox',
    'id' : 'checkbox_anon',
}

attrs_event ={
    'class' : 'form-control',
    'type' : 'text',
}

class Donation_Form(forms.Form):
    list_event = []
    def __init__(self):
        events = Event.objects.all()
        for event in events:
            self.list_event.append((event.title, event.title))

    event = forms.CharField(label='Program Name', widget=forms.Select(choices=list_event, attrs=attrs_event))
    full_name = forms.CharField(label = "Full Name", required=True, max_length=70, empty_value="", widget = forms.TextInput(attrs= attrs_name))
    email = forms.EmailField(label = "E-mail", required=True, max_length=30, empty_value="", widget = forms.TextInput(attrs= attrs_email))
    amount = forms.DecimalField(label = "Amount", required=True, max_digits=15, decimal_places=0, widget = forms.TextInput(attrs= attrs_money))
    anonymous = forms.BooleanField(label = "Donate Anonymously", required=False, widget = forms.CheckboxInput(attrs= attrs_anon))

    def clean_email(self):
        email = self.cleaned_data['email']
        (email_base, provider) = email.split("@")
        (domain, extension) = provider.split(".")
        # Check if email is a valid format
        if not ((domain == "gmail" or domain == "yahoo") and (extension == 'com' )):
            raise ValidationError("{0} isn't a valid email. Please use a valid email address".format(email))

        # Check if email is registered in database
        allUser = User.objects.all()
        registered = False
        for user in allUser:
            if (user.email == self.cleaned_data['email']):
                registered = True
        if (not registered) :
            raise ValidationError("Email is not registered")
        return email

    def clean_amount(self):
        amount = self.cleaned_data['amount']
        if(amount <= 0):
            raise ValidationError('Please input the right amount of donation')
        return amount
