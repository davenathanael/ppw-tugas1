from django.test import TestCase
import datetime
from data.models import Donatur, Event, User
# from donation_page.models import Event
from .forms import Donation_Form
from .views import submit_input
from django.test import Client
from django.urls import resolve
# from RegistrationForm_Page.models import User

# Create your tests here.
class DonationForm_Test(TestCase):

    def test_DF_url_is_exist(self):
        response =  Client().get('/donationform_page/')
        self.assertEqual(response.status_code, 200)

    # def test_DF_using_donationform_func(self):
    #     found = resolve('/donationform_page')
    #     self.assertEqual(found.func, submit_input)

    def test_DF_using_submitinput_func(self):
        found = resolve('/donationform_page/submitdata')
        self.assertEqual(found.func, submit_input)

    def test_DF_model_can_create_event_(self):
        # Creating a new activity (sebagai tes)
        timeNow = datetime.datetime.now()
        new_event = Event.objects.create(category = "Pendidikan", viewed = 5, contributor = "PT Gulaku",
                    title = "Donasi Buku", desc = "Ayo berdonasi", total = 10, due = timeNow, target=0)
        # Retrieving all available activity
        counting_all_available_event = Event.objects.all().count()
        self.assertEqual(counting_all_available_event, 1)

    def test_DF_model_can_create_donatur_(self):
        # Creating a new activity (sebagai tes)
        timeNow = datetime.datetime.now()
        new_event = Event.objects.create(category = "Pendidikan", viewed = 5, contributor = "PT Gulaku",
                    title = "Donasi Buku", desc = "Ayo berdonasi", total = 10, due = timeNow, target=0)
        new_donatur = Donatur.objects.create(time = timeNow, amount =10000,
                    event = new_event, anonymous=True)
        self.assertEqual(timeNow, new_donatur.time)
        self.assertEqual(10000, new_donatur.amount)
        self.assertTrue(new_donatur.anonymous)
        self.assertIsInstance(new_donatur.event, Event)

    def test_DF_form_validation_is_email_registered(self):
        # Not registered in database
        form = Donation_Form(data={'event' : 'Donasi Palu', 'full_name': 'Celine',
                              'email': 'chrysantcc@gmail.com', 'amount':10})
        self.assertFalse(form.is_valid())

    # def test_DF_form_validation_for_amount(self):
    #     User.objects.create(username = "nabilafv", full_name = 'Celine', birth_date = "1999-02-03",
    #                         email = "test@gmail.com", password = "yourmentalhealthmatters")
    #     Event.objects.create(category = "Pendidikan", viewed = 5, contributor = "PT Gulaku",
    #                         title = "Donasi Buku", desc = "Ayo berdonasi", total = 10,
    #                         due = datetime.datetime.now(), target=0)
    #     form = Donation_Form(data={'event' : 'Donasi Buku', 'full_name': 'Celine',
    #                           'email': 'test@gmail.com', 'amount':-10})
    #     self.assertFalse(form.is_valid())

    def test_DF_post_success_and_render_the_result(self):
        test = 'Anonymous'
        # If input is posted successfully, the page will redirect to '/donation/(dummy_event.id)'
        User.objects.create(username = "nabilafv", full_name = test, birth_date = "1999-02-03",
                            email = "test@gmail.com", password = "yourmentalhealthmatters")
        Event.objects.create(category = "Pendidikan", viewed = 5, contributor = "PT Gulaku",
                            title = "Donasi Buku", desc = "Ayo berdonasi", total = 10,
                            due = datetime.datetime.now(), target=0)
        response_post = Client().post('/donationform_page/submitdata', {'event':'Donasi Buku' ,
                                        'full_name': test, 'email': 'test@gmail.com',
                                        'amount': 100, 'anonymous': False})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/donationform_page/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_DF_post_error_and_render_the_result(self):
        test = 'Date'
        response_post = Client().post('/donationform_page/submitdata', {'event':'',
                                    'full_name': '', 'email': '', 'amount': 0, 'anonymous': ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/donationform_page')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
