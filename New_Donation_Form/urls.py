from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.submit_input, name = "DonationForm"),
    path("submitdata", views.submit_input, name = "DonationForm_Submit")
]