from django.shortcuts import render, HttpResponseRedirect
from .forms import Donation_Form
from django import forms
# from .models import Donatur
from data.models import Event, Donatur

# Create your views here.

# def donationform(request):
#     request.POST['author'] = "Chrysant Celine Setyawan"
#     request.POST['donation_form'] = Donation_Form
#     return render (request, "donationform_page.html", request.POST)

def submit_input(request):
    form = Donation_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        # Get event as requested by user
        dummy_event = Event.objects.all().get(title = request.POST['event'])
        # Count total donation
        dummy_event.total = dummy_event.total + int(request.POST['amount'])
        dummy_event.save()

        # If you have not specified a value attribute in the checkbox HTML element,
        # the default value which will passed in the POST is on if the checkbox is checked.
        donate = Donatur(full_name = request.POST['full_name'], email = request.POST['email'],
                        event = dummy_event, amount = request.POST['amount'],
                        anonymous = request.POST.get('anonymous', False) == 'on')

        donate.save()

        return HttpResponseRedirect('/donations/' + str(dummy_event.id))
    else :
        return render(request, "donationform_page.html", {'author': 'Test', 'donation_form': form})
