# Crowd Funding
![Pipeline](https://gitlab.com/davenathanael/ppw-tugas1/badges/master/pipeline.svg) ![Coverage](https://gitlab.com/davenathanael/ppw-tugas1/badges/master/coverage.svg)

Crowd Funding sebagai tugas 1 PPW 2018 kelas B dengan anggota kelompok:
1. Dhipta Raditya Yudhasmara (1706028644)
2. Nabila Febri Viola (1706039654)
3. Chrysant Celine Setyawan (1706039862)
4. Dave Nathanael (1706040076)

Aplikasi *live* dapat diakses di *link* Heroku berikut:
http://crowd-funding-ppw.herokuapp.com/


