from django.apps import AppConfig


class RegistrationformPageConfig(AppConfig):
    name = 'RegistrationForm_Page'
