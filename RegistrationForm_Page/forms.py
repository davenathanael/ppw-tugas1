from django.core.exceptions import ValidationError
from django import forms
from data.models import User

attrs_username = {
    'class': 'form-control',
    'placeholder': 'Input your username here',
    'pattern': '[A-Za-z0-9_@+.-]+',
    'title': 'Only alphanumeric characters and (_, @, +, ., -) are allowed.',
}

attrs_fullName = {
    'class': 'form-control',
    'placeholder': 'Input your full name here',
}

attrs_birthDate = {
    'class': 'form-control',
    'placeholder': 'YYYY-MM-DD',
}

attrs_email = {
    'class': 'form-control',
    'placeholder': 'Input your e-mail here',
}

attrs_password = {
    'class': 'form-control',
    'placeholder': 'Input your password here',
}

class Registration_Form(forms.Form):
    username = forms.CharField(label="Username", required=True, max_length=50, widget=forms.TextInput(attrs=attrs_username))
    full_name = forms.CharField(label="Full Name", required=True, max_length=70, widget=forms.TextInput(attrs=attrs_fullName))
    birth_date = forms.DateField(label="Birth Date", required=True, widget=forms.DateInput(attrs=attrs_birthDate))
    email = forms.EmailField(label="E-mail", required=True, widget=forms.EmailInput(attrs=attrs_email))
    password = forms.CharField(max_length=128, widget=forms.PasswordInput(attrs=attrs_password))

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError("{0} already exists".format(email))
        return email
