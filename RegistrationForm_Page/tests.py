from django.test import TestCase
from django.test import Client
from django.urls import resolve
from data.models import User
from .forms import Registration_Form
from .views import registrationForm as index

# Create your tests here.
class RegistrationForm_Test(TestCase):
    def test_registration_url_is_exist(self):
        response = Client().get('/registration/')
        self.assertEqual(response.status_code, 200)

    def test_registration_using_index_func(self):
        found = resolve('/registration/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_user(self):
        # Creating a new user
        new_user = User.objects.create(username = "nabilafv", full_name = "Nabila Febri Viola",
                                       birth_date = "1999-02-03", email = "nabila.febri@ui.ac.id",
                                       password = "yourmentalhealthmatters")
        # Retrieving all available user
        counting_all_available_user = User.objects.all().count()
        self.assertEqual(counting_all_available_user, 1)

    def test_form_validation_for_blank_items(self):
        form = Registration_Form(data={'username': '', 'full_name': '', 'birth_date': '', 'email': '', 'password': ''})
        self.assertFalse(form.is_valid())

    def test_form(self):
        form = Registration_Form(data={'username': "nabilafv", 'full_name': "Nabila Febri Viola",
                                       'birth_date': "1999-02-03", 'email': "nabila.febri@ui.ac.id",
                                       'password': "yourmentalhealthmatters"})
        self.assertTrue(form.is_valid())

    def test_post(self):
        response = self.client.post('/registration/', {'username': "nabilafv", 'full_name': "Nabila Febri Viola",
                                       'birth_date': "1999-02-03", 'email': "nabila.febri@ui.ac.id",
                                       'password': "yourmentalhealthmatters"})
        self.assertEqual(response.status_code, 200)
