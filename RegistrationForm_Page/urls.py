from django.urls import path
from .views import registrationForm

urlpatterns = [
	path('', registrationForm, name='registrationForm'),
]