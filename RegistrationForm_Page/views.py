from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Registration_Form
from data.models import User

response = {}

# Create your views here.
def registrationForm(request):
    users = User.objects.all().values()
    if (request.method == 'POST'):
        form = Registration_Form(request.POST)
        if (form.is_valid()):
            response['username'] = request.POST['username']
            response['full_name'] = request.POST['full_name']
            response['birth_date'] = request.POST['birth_date']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']
            user = User(username=response['username'], full_name=response['full_name'],
                        birth_date=response['birth_date'], email=response['email'],
                        password=response['password'])
            user.save()
            HttpResponseRedirect('/')
    else:
        form = Registration_Form()
    response['form'] = form
    response['users'] = users
    return render(request, 'registrationForm.html', response)
