from django.test import TestCase
from django.urls import resolve
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest


class HomePageTest(TestCase):
    def test_correct_template(self):
        res = self.client.get('/about')
        self.assertTemplateUsed(res, 'about.html')

    def test_status_code(self):
        res = self.client.get('/about')
        self.assertEqual(200, res.status_code)

    def test_resolve_function(self):
        found = resolve('/about')
        self.assertEqual(found.func, index)

# class FunctionalTest(unittest.TestCase):
#     url = 'http://localhost:8000'
#     # url = 'http://lab-with-tests.herokuapp.com'
#
#     def setUp(self):
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome(chrome_options=options)
#
#     def tearDown(self):
#         self.browser.quit()
#
#     def test_post(self):
#         res = self.browser.get(self.url + '/auth/login/google-oauth2/')
#         idInput = self.browser.find_element_by_id('identifierId')
#         idInput.send_keys('dummyppwb@gmail.com')
#         idInput.send_keys(Keys.ENTER)
#
#         pwInput = self.browser.find_element_by_tag_name('input')
#         pwInput.send_keys('dummydummy')
#         pwInput.send_keys(Keys.ENTER)
#
#         res = self.browser.get(self.url + '/about')
#         testiInput = self.browser.find_element_by_id('testimony-input')
#         testiInput.send_keys('Test')
#
#         testiButton = self.browser.find_element_by_tag_name('button').click()
#         # testiButton.send_keys(Keys.ENTER)
