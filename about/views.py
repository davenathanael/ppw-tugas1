from django.shortcuts import render
from django.http import JsonResponse
from data.models import Testimony
import json

imgUrl = 'https://us.123rf.com/450wm/martialred/martialred1608/martialred160800018/61263271-user-account-profile-circle-flat-icon-for-apps-and-websites.jpg?ver=6'


def index(req):
    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)
        Testimony.objects.create(name=str(req.user), imgUrl=imgUrl, content=data['content'])
        user = {
            'name': str(req.user),
            'imgUrl': imgUrl,
        }
        return JsonResponse({'user': user})
    else:
        testimonies = Testimony.objects.all().values()
        loggedIn = req.user
        if (str(loggedIn) == 'AnonymousUser'):
            loggedIn = False
        resp = {
            'testimonies': testimonies,
            'loggedin': loggedIn,
        }
        return render(req, 'about.html', resp)
