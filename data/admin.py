from django.contrib import admin
from .models import Testimony, Event, Donatur, User
# Register your models here.
admin.site.register(Testimony)
admin.site.register(Event)
admin.site.register(Donatur)
admin.site.register(User)
