from django.db import models
from django.utils import timezone

class Testimony(models.Model):
    name = models.CharField(max_length=40)
    imgUrl = models.CharField(max_length=40)
    content = models.CharField(max_length=100)

class Event(models.Model):
    title = models.CharField(max_length=300)
    short_desc = models.CharField(max_length=100)
    desc = models.TextField(max_length=2000)
    category = models.CharField(max_length=15)
    viewed = models.DecimalField(max_digits=10, decimal_places=0)
    contributor = models.CharField(max_length=20)
    total = models.DecimalField(max_digits=15, decimal_places=0)
    target = models.DecimalField(max_digits=15, decimal_places=0)
    due = models.DateTimeField(default=timezone.now())
    image_src = models.CharField(max_length=2393, default="")

    def get_absolute_url(self):
        return reverse('index', kwargs={'id':self.id})

class Donatur(models.Model):
    full_name = models.CharField(max_length = 70, default='Input your name here')
    email = models.EmailField(max_length = 30, default='Input your e-mail here')
    time = models.DateTimeField(default=timezone.now())
    amount = models.DecimalField(max_digits=15, decimal_places=0)
    event = models.ForeignKey(Event, on_delete = models.CASCADE)
    anonymous = models.BooleanField(default=False)

class User(models.Model):
    username = models.CharField(max_length=50)
    full_name = models.CharField(max_length=70)
    birth_date = models.DateField()
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=128)
