from django.apps import AppConfig


class DonationPageConfig(AppConfig):
    name = 'donation_page'
