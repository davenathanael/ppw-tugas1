from django.db import models
from django.urls import reverse
from django.utils import timezone

# Create your models here.
# class Event(models.Model):
#     # event_id = models.DecimalField(max_digits=10, decimal_places=0, default=0)
#     title = models.CharField(max_length=300)
#     short_desc = models.CharField(max_length=100)
#     desc = models.TextField(max_length=2000)
#     category = models.CharField(max_length=15)
#     viewed = models.DecimalField(max_digits=10, decimal_places=0)
#     contributor = models.CharField(max_length=20)
#     total = models.DecimalField(max_digits=15, decimal_places=0)
#     target = models.DecimalField(max_digits=15, decimal_places=0)
#     due = models.DateTimeField(default=timezone.now())
#     image_src = models.CharField(max_length=2393, default="")
#     # icon = models.CharField(max_length=2393, default="")
#     #donaturs = models.OneToManyField(to = Donatur)
#
#     def get_absolute_url(self):
#         return reverse('index', kwargs={'id':self.id})

# class Donatur(models.Model):
#     # donatur_id = models.DecimalField(max_digits=10, decimal_places=0, default=0)
#     # user = models.ForeignKey(User, on_delete = models.CASCADE)
#     name = models.CharField(max_length=100, default='joko')
#     time = models.DateTimeField(default=timezone.now())
#     amount = models.DecimalField(max_digits=15, decimal_places=0)
#     event = models.ForeignKey(Event, on_delete = models.CASCADE)
#     anonymous = models.BooleanField()
