from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone

from data.models import Event, Donatur
from .views import index
class donation_page(TestCase):
	def test_get_absolute_url(self):
		dummyString = "lol"
		ev = Event.objects.create(title=dummyString, short_desc=dummyString, desc=dummyString, category=dummyString, viewed=0, contributor=dummyString, total=0, target=0, due=timezone.now())
		obj_id = Event.objects.all().latest('due').id
		self.assertEqual(ev.get_absolute_url(),'/donations/'+ str(obj_id) +'/')


	def test_url_dynamic_exist(self):
		dummyString = "lol"
		ev = Event.objects.create(title=dummyString, short_desc=dummyString, desc=dummyString, category=dummyString, viewed=0, contributor=dummyString, total=0, target=0, due=timezone.now())
		obj_id = Event.objects.all().latest('due').id
		response = Client().get('/donations/'+str(obj_id)+'/')
		self.assertEqual(response.status_code, 200)

	def test_url_dynamic_not_exist(self):
		dummyString = "lol"
		ev = Event.objects.create(title=dummyString, short_desc=dummyString, desc=dummyString, category=dummyString, viewed=0, contributor=dummyString, total=0, target=0, due=timezone.now())
		obj_id = Event.objects.all().latest('due').id + 1
		response = Client().get('/donations/'+str(obj_id)+'/')
		self.assertNotEqual(response.status_code, 200)


	def test_url_dynamic_contains(self):
		dummyString = "lol"
		ev = Event.objects.create(title=dummyString, short_desc=dummyString, desc=dummyString, category=dummyString, viewed=0, contributor=dummyString, total=0, target=0, due=timezone.now())
		obj_id = Event.objects.all().latest('due').id
		response = self.client.get('/donations/'+str(obj_id)+'/')
		html_response = response.content.decode('utf8')
		self.assertIn('lol', html_response)

	def test_url_dynamic_not_contains(self):
		dummyString = "lol"
		ev = Event.objects.create(title=dummyString, short_desc=dummyString, desc=dummyString, category=dummyString, viewed=0, contributor=dummyString, total=0, target=0, due=timezone.now())
		obj_id = Event.objects.all().latest('due').id
		response = self.client.get('/donations/'+str(obj_id)+'/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('kak pewe', html_response)
