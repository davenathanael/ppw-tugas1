from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect

from data.models import Event, Donatur
# Create your views here.

def index(request, id):
	if(int(id) > Event.objects.all().count()):
		return HttpResponseRedirect('/')

	obj = get_object_or_404(Event, id=id)
	people = Donatur.objects.filter(event = obj)
	return render(request, 'index.html', {'data':obj, 'people':people})
