from django.test import TestCase, Client
from django.urls import resolve

from .views import *

# Create your tests here.
class googleLogin_Test(TestCase):
    def test_sign_in_url_is_exist(self):
        response = Client().get('/donor/sign-in/')
        self.assertEqual(response.status_code, 200)

    def test_googleLogin_using_sign_in_func(self):
        found = resolve('/donor/sign-in/')
        self.assertEqual(found.func, sign_in)

    def test_sign_out_url_is_exist(self):
        response = Client().get('/donor/sign-out/')
        self.assertEqual(response.status_code, 302)

    def test_googleLogin_using_sign_out_func(self):
        found = resolve('/donor/sign-out/')
        self.assertEqual(found.func, sign_out)

    def test_googleLogin_using_googleLogin_template(self):
        response = self.client.get('/donor/sign-in/')
        self.assertTemplateUsed(response, 'googleLogin.html')