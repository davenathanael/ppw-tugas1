from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import logout

# Create your views here.
response = {}
def sign_in(request):
    if request.user.is_authenticated:
        if 'name' not in request.session:
            request.session['name'] = request.user.first_name + " " + request.user.last_name
        if 'username' not in request.session:
            request.session['username'] = request.user.username
        if 'email' not in request.session:
            request.session['email'] = request.user.email
        return render(request, "googleLogin.html",response)
    return render(request, "googleLogin.html")

def sign_out(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/')
