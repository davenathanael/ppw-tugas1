from django.test import TestCase
from django.urls import resolve
from .views import index

class HomePageTest(TestCase):
    def test_correct_template(self):
        res = self.client.get('/')
        self.assertTemplateUsed(res, 'home.html')

    def test_status_code(self):
        res = self.client.get('/')
        self.assertEqual(200, res.status_code)
        res = self.client.get('/?filter=viewed')
        self.assertEqual(200, res.status_code)
        res = self.client.get('/?filter=recentdue')
        self.assertEqual(200, res.status_code)

    def test_resolve_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
