from django.shortcuts import render
from data.models import Event

def index(req):

    programs = Event.objects.all()
    filter = req.GET.get('filter')
    if (filter == 'viewed'):
        programs = Event.objects.all().order_by('-viewed')
    elif (filter == 'recentdue'):
        programs = Event.objects.all().order_by('-due')

    return render(req, 'home.html', {'programs': programs})
