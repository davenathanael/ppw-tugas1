const debug = false
const dev = false

const postTestimony = async (event, title) => {
  event.preventDefault(true)
  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const content = document.getElementById('testimony-input').value.trim()
  if (debug) console.table([token, content])

  if (content === '') return;

  const url = dev ? 'http://localhost:8000/about' : 'http://crowd-funding-ppw.herokuapp.com/about'
  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({ content })
  }

  const json = await (await fetch(url, opt)).json()

  if (json.error) {
    console.error(json.error)
  } else {
    const wrapper = document.getElementById('testimonies')
    const newTestimony = document.createElement('div')
    newTestimony.classList.add('testi')

    const img = document.createElement('img')
    img.setAttribute('src', json.user.imgUrl)
    img.setAttribute('alt', 'Profile Picture')

    const div = document.createElement('div')

    const pName = document.createElement('p')
    pName.innerText = json.user.name
    pName.style.textDecoration = 'underline';

    const pContent = document.createElement('p')
    pContent.innerText = content

    div.appendChild(pName)
    div.appendChild(pContent)

    newTestimony.appendChild(img)
    newTestimony.appendChild(div)

    wrapper.appendChild(newTestimony)
    document.getElementById('testimony-input').value = '';
  }
}
