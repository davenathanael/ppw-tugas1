function onSignIn(googleUser) {
    console.log("on sign in, granted scopes: " + googleUser.getGrantedScopes());
    console.log("ID token: " + googleUser.getAuthResponse().id_token);
    var profile = googleUser.getBasicProfile();
    console.log(profile.getEmail());
    console.log(profile.getName());

}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
        alert("You have been successfully signed out!");
        console.log('User signed out.');
        location.assign('/');
    });
}

function onLoad() {
  gapi.load('auth2', function() {
    var auth2 = gapi.auth2.init({
        client_id: '602226791509-abivehbi7a4qjejha826d61h07v3udkj.apps.googleusercontent.com',
        fetch_basic_profile: true,
        scope: 'profile email openid'
    });

    // Sign the user in, and then retrieve their ID.
      auth2.signIn().then(function() {
        console.log(auth2.currentUser.get().getId());
      });
  });
}