from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import user_profile
from data.models import Donatur, User
# from RegistrationForm_Page.models import User
from django.contrib.auth import get_user_model

# Create your tests here.
class UserProfile_Test(TestCase):

    def test_user_profile_url_is_exist(self):
        response =  Client().get('/profile')
        self.assertEqual(response.status_code, 301)

    # def test_user_can_login(self):
    #     User_Django = get_user_model()
    #     user = User_Django(username= 'test', email="test@gmail.com", password='test123')
    #     user.full_clean()
    #     if(user.is_authenticated):
    #         email_login = user.email
    #         datas = Donatur.objects.all()
    #         new_datas = []
    #         response = {}
    #         for data in datas:
    #             if(data.email == email_login):
    #                 new_datas.append(data)
    #         response['datas'] = new_datas
    #     program_donation = new_datas[0].event.title
    #     self.assertIn(program_donation, 'profile.hml')
