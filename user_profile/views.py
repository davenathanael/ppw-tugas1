from django.shortcuts import render, HttpResponseRedirect
from data.models import User, Donatur
# from New_Donation_Form.models import Donatur

# Create your views here.
def user_profile(request):
    if(request.user.is_authenticated):
        email_login = request.user.email
        datas = Donatur.objects.all()
        new_datas = []
        response = {}
        for data in datas:
            if(data.email == email_login):
                new_datas.append(data)
        response['datas'] = new_datas
        return render(request, 'profile.html', response)
    else:
        return HttpResponseRedirect('/')
